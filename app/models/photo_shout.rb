class PhotoShout < ActiveRecord::Base
	has_attached_file :image, styles: {
		shout: "400X400>"
	}
end
