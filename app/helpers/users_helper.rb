module UsersHelper
	def follow_button user
		if current_user == user
			return
		elsif current_user.following? user
			button_to "Unfollow", user_follow_path(user), 
													method: :delete,
													class:"btn btn-danger"
		else
			button_to "Follow", user_follow_path(user), class:"btn btn-success"
		end
	end
end