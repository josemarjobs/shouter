require "digest/md5"
module ShoutsHelper
	def gravatar(user, size = 48, options = {})
		digest = Digest::MD5.hexdigest(user.email)
		url = "http://gravatar.com/avatar/#{digest}?s=#{size}"
		image_tag(url, options)
	end
end